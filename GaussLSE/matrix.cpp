#include "matrix.h"
#include <iostream>
#include <stdexcept>

using namespace std;

template<typename T>
void Matrix<T>::Set(const T& elem, unsigned const row, unsigned const clm)
{
    (*matrix)[row * rows + clm] = elem;
}

template<typename T>
T& Matrix<T>::Get(unsigned const row, unsigned const clm)
{
    return (*matrix)[row * rows + clm];
}

template<typename T>
T& Matrix<T>::At(unsigned const row, unsigned const clm)
{
    return Get(row, clm);
}

template<typename T>
unsigned Matrix<T>::GetRank() const
{
    return 1;
}

template<typename T>
void Matrix<T>::Transpose()
{
    for (unsigned i = 0 ; i < rows ;)
    {
        for (unsigned j = i + 1 ; j < columns ;)
        {
            T& ij = At(i, j);
            T& ji = At(j, i);
            T tmp = ij;
            ij = ji;
            ji = tmp;

            ++j;
        }
        ++i;
    }
}

template<typename T>
void Matrix<T>::MakeGauss(lua_State* L)
{
    lua_getglobal(L, "makeGauss");

    T* values = &((*matrix)[0]);
    PushNumberTableOnStack(L, values, this->matrix->size());

    lua_pushnumber(L, columns);
    lua_setfield(L, -2, "x");

    lua_pushnumber(L, rows);
    lua_setfield(L, -2, "y");

    if (lua_pcall(L, 1, 1, 0) != 0)
    {
         throw std::runtime_error(lua_tostring(L, -1));
    }

    size_t size = GetTableSize(L, -1);
    values = GetNumberTableFromStack(L);
    delete this->matrix;
    this->matrix = new vector<T>(values, values + size);
    rows = size / columns; // may be less rows than on start
}

template<typename T>
T& Matrix<T>::operator() (unsigned const i, unsigned const j)
{
    return this->Get(i, j);
}

template<typename T>
Matrix<T>& Matrix<T>::operator= (const Matrix<T>& that)
{
    if (this != &that)
    {
        this->rows = that.rows;
        this->columns = that.columns;
        delete matrix;
        this->matrix = new vector<T>(*(that.matrix));
    }

    return *this;
}

template<typename T>
void Matrix<T>::InitLua(lua_State* L)
{
    if (luaL_loadfile(L, "GaussLSE.lua") != 0)
    {
        printf("Error loading .lua\n");
        throw std::runtime_error("Error loading .lua");
    }

    if (lua_pcall(L, 0, 0, 0))
    {
        printf("pcall failed");
        throw std::runtime_error("Error loading .lua");
    }
}

template<typename T>
void Matrix<T>::Print()
{
    for (unsigned i = 0 ; i < this->rows ;)
    {
        for (unsigned j = 0 ; j < this->columns; )
        {
            cout << this->At(i, j) << " ";
            ++j;
        }
        cout << endl;
        ++i;
    }
}


template<typename T>
Matrix<T>::~Matrix()
{
    delete matrix;
}
