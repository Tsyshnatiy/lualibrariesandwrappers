TEMPLATE = app
CONFIG += console
CONFIG -= qt

QMAKE_CXXFLAGS +=-std=c++0x

LIBS += -llua
LIBS += -ldl

SOURCES += main.cpp \
    luatableapi.cpp \
    matrix.cpp

OTHER_FILES += \
    GaussLSE.lua

HEADERS += \
    luatableapi.h \
    matrix.h

