local function printMatrix(matrix)
  --assert(matrix['x'] or matrix['y'] == nil, "no sizes in matrix")
  --assert(#matrix == matrix['x'] * matrix['y'], "Non valid matrix")
  local sizeX = matrix['x']
  local sizeY = matrix['y']
  local i = 1
  while (i <= sizeY) do
    local j = 1
    while (j <= sizeX) do
      if (type(matrix[sizeX * (i - 1) + j]) == "number") then
        io.write(matrix[sizeX * (i - 1) + j])
        io.write(" ")
      end
      j = j + 1
    end
    print()
    i = i + 1
  end
end

local function getFirstRowElem(matrix, rowNumber)
  return (rowNumber - 1) * matrix['x'] + 1
end

local function swapRows(matrix, row1, row2)
  local idx1 = getFirstRowElem(matrix, row1)
  local idx2 = getFirstRowElem(matrix, row2)
  local sizeX = matrix['x']
  
  local idx = 0
  while (idx < sizeX) do
    matrix[idx1 + idx], matrix[idx2 + idx] = matrix[idx2 + idx], matrix[idx1 + idx]
    idx = idx + 1
  end
end

local function getFirstNonNull(matrix, rowNumber)
  local idx = getFirstRowElem(matrix, rowNumber)
  local sizeX = matrix['x']
  local beginPos = idx
  while (idx < beginPos + sizeX) do
  
    if (matrix[idx] ~= 0) then
      return idx;
    end
    
    idx = idx + 1
  end
  
  return nil
end

local function getNon0AndRemove0Rows(matrix, rowIdx)
    local nonNull = nil
    while (nonNull == nil) do
      if (matrix['y'] == 0) then
        return nil
      end
      
      nonNull = getFirstNonNull(matrix, rowIdx)
      if (nonNull == nil) then
        if (rowIdx ~= matrix['y']) then
          swapRows(matrix, rowIdx, matrix['y'])
          matrix['y'] = matrix['y'] - 1
        else
          matrix['y'] = matrix['y'] - 1
          break;
        end
      end
    end
    
    return nonNull
end

local function normalizeRow(matrix, rowNumber)
  local idxNonNull = getFirstNonNull(matrix, rowNumber)
  if (idxNonNull == nil)then
    return
  end
  
  local firstRowElem = getFirstRowElem(matrix, rowNumber)
  
  local divider = matrix[idxNonNull]
  
  local idx = 0
  while (idx < matrix['x']) do
    matrix[firstRowElem + idx] = matrix[firstRowElem + idx] / divider
    idx = idx + 1
  end
end

local function transformRow(matrix, row1, row2, multiplier)
  local idx1 = getFirstRowElem(matrix, row1)
  local idx2 = getFirstRowElem(matrix, row2)
  
  local idx = 0
  local sizeX = matrix['x']
  
  while(idx < sizeX) do
    matrix[idx2 + idx] = multiplier * matrix[idx1 + idx] + matrix[idx2 + idx]
    
    if (math.abs(matrix[idx2 + idx]) < 1e-6) then
      matrix[idx2 + idx] = 0
    end
    
    idx = idx + 1
  end
end

local function makeDirectGauss(matrix)
  local i = 1;
  while (i <= matrix['y']) do
    local iNon0 = getNon0AndRemove0Rows(matrix, i)
    local j = i + 1
    while (j <= matrix['y']) do
      local jNon0 = getFirstNonNull(matrix, j)
      
      if (iNon0 ~= nil and jNon0 ~= nil and (iNon0 - jNon0) % matrix['x'] == 0) then
        local multiplier = -1 * matrix[jNon0] / matrix[iNon0]
        transformRow(matrix, i, j, multiplier)
      end
      j = j + 1
    end
    normalizeRow(matrix, i)
    i = i + 1
  end
  
  return matrix
end

local function makeInversedGauss(matrix)
  local i = matrix['y']
  local sizeX = matrix['x']
  while (i > 1) do
    local j = i - 1
    while (j >= 1) do
      local iNon0 = getFirstNonNull(matrix, i)
      local elemToZero = matrix[iNon0 - (i - j) * sizeX]
      if (elemToZero ~= 0) then
        local multiplier = -1 * elemToZero / matrix[iNon0]
        transformRow(matrix, i, j, multiplier)
      end 
      
      j = j - 1
    end
    
    i = i - 1
  end
  return matrix
end

function makeGauss(matrix)
  assert(matrix['x'] or matrix['y'] == nil, "No sizes in matrix")
  assert(#matrix == matrix['x'] * matrix['y'], "Invalid data")

  return makeInversedGauss(makeDirectGauss(matrix))
end

local function main()
  local m = {x = 4, y = 3, 
            244, 77, 88, 0, 
            0, 0, 0 ,0,
            1031, 21231312,  2131, 123132}
            
  printMatrix(m)
  makeGauss(m)
  print()
  printMatrix(m)
end

--main()
