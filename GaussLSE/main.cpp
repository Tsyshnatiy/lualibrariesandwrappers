#include <iostream>
#include <matrix.h>
#include <matrix.cpp>
#include <algorithm>

using namespace std;

int main()
{
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);

    Matrix<double> m1(L, 2, 3, true);

    m1.Set(1, 0, 0);
    m1.Set(3, 0, 1);
    m1.Set(7, 0, 2);

    m1.Set(3, 1, 0);
    m1.Set(21, 1, 1);
    m1.Set(13, 1, 2);

    m1.Print();
    m1.MakeGauss(L);

    cout << endl;

    m1.Print();
    return 0;
}

