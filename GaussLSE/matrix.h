#ifndef MATRIX_H
#define MATRIX_H

#include <vector>
#include <luatableapi.h>

using namespace std;

template<typename T>
class Matrix
{
private:
    vector<T>* matrix;
    unsigned rows;
    unsigned columns;

    void InitLua(lua_State* L);
public:
    //Creates empty matrix
    Matrix(lua_State* L, unsigned const r, unsigned const c, bool needInit = false)
        : rows(r), columns(c)
    {
        matrix = new vector<T>(rows * columns);

        if (needInit)
            InitLua(L);
    }

    //Creates matrix from 1-dim array.
    //Check collection sizes by yourself or you will
    //get segfault
    Matrix(const T* const collection, lua_State* L, unsigned const r, unsigned const c, bool needInit = false)
    {
        matrix = new vector<T>(collection, collection + r * c);
        rows = r;
        columns = c;

        if (needInit)
            InitLua(L);
    }

    //Wraps 2-dim array by the matrix.
    //Check collection sizes by yourself or you will
    //get segfault
    Matrix(const T** collection, lua_State* L, unsigned const r, unsigned const c, bool needInit = false)
        : Matrix(L, r, c, needInit)
    {
        for (unsigned i = 0 ; i < rows ;)
        {
            for (unsigned j = 0 ; j < columns ;)
            {
                matrix->push_back(collection[i][j]);
                ++j;
            }
            ++i;
        }
    }

    //Copy constructor
    Matrix(const Matrix& that)
    {
        matrix = new vector<T>();
        *this = that;
    }

    //TODO impl move semantics. Wait for GNU when std::move bug will be fixed.

    unsigned GetRowSize() const { return rows; }
    unsigned GetClmSize() const { return columns; }

    pair<unsigned const, unsigned const> GetSizes() const { return make_pair(rows, columns); }

    void Set(const T& elem, unsigned const row, unsigned const clm);
    T& Get(unsigned const row, unsigned const clm);
    T& At(unsigned const row, unsigned const clm);

    unsigned GetRank() const;

    void Transpose();
    void MakeGauss(lua_State* L);

    void Print();

    Matrix<T>& operator= (const Matrix<T>& that);
    inline T& operator() (unsigned const i, unsigned const j); //used instead of [i][j]

    ~Matrix();
};

#endif // MATRIX_H
