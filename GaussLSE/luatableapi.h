#ifndef LUATABLEHELPER_H
#define LUATABLEHELPER_H

#include <stdio.h>

extern "C"
{
    #include <lua.h>
    #include <lauxlib.h>
    #include <lualib.h>
}

size_t GetTableSize(lua_State* L, int stackIdx);

void PushNumberTableOnStack(lua_State* L,
                      const double* values,
                      const int valuesSize);

void PushStringTableOnStack(lua_State* L,
                      char** values,
                      const int valuesSize);

double* GetNumberTableFromStack(lua_State* L);
char** GetStringTableFromStack(lua_State* L);

#endif // LUATABLEHELPER_H
