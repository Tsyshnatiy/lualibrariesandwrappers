#include <stdio.h>
#include <string.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

static lua_State *L;

double add(double x, double y)
{
    double z;
    /* push functions and arguments */
    lua_getglobal(L, "add");  /* function to be called */
    lua_pushnumber(L, x);   /* push 1st argument */
    lua_pushnumber(L, y);   /* push 2nd argument */
    int error;
    /* do the call (2 arguments, 1 result) */
    if ((error = lua_pcall(L, 2, 1, 0)) != 0)
    {
        printf("pcall error: %s\n",  lua_tostring(L, -1));
        return 1;
    }

    /* retrieve result */
    if (!lua_isnumber(L, -1))
    {
        printf("non number error\n");
        return 0;
    }

    z = lua_tonumber(L, -1);
    lua_pop(L, 1);  /* pop returned value */
    return z;
}

int main(void)
{
    L = luaL_newstate();
    luaL_openlibs(L);
    if (luaL_dofile(L, "add.lua") != 0)
    {
        printf("Error loading .lua\n");
        return 1;
    }

    double res = add(10, 2);
    printf("Res is %lf\n", res);
    lua_close(L);
    return 0;
}
