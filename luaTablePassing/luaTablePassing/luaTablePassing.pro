TEMPLATE = app
CONFIG += console
CONFIG -= qt

QMAKE_CFLAGS += -std=c++11

LIBS += -llua
LIBS += -ldl

SOURCES += \
    luatableapi.cpp \
    main.cpp

HEADERS += \
    luatableapi.h
