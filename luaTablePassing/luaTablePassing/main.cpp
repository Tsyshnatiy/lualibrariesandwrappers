#include <stdio.h>
#include <string.h>

extern "C"
{
    #include <lua.h>
    #include <lauxlib.h>
    #include <lualib.h>
}

#include <stdlib.h>
#include <luatableapi.h>

#define NUMBER

#ifdef NUMBER
#define TABLE_TYPE double
#else
#define TABLE_TYPE char*
#endif

#define TABLE_SIZE 10

static lua_State *L;

static void Init(TABLE_TYPE*& table)
{
    table = (TABLE_TYPE*) malloc(TABLE_SIZE * sizeof(TABLE_TYPE));
#ifdef NUMBER
    TABLE_TYPE el = 100;
    for (int i = 0 ; i < TABLE_SIZE ;)
    {
        *(table + i) = el++;
        ++i;
    }
#else
    char el = 'A';
    for (int i = 0 ; i < TABLE_SIZE;)
    {
        *(table + i) = (TABLE_TYPE) malloc(2 * sizeof(char));
        table[i][0] = el++;
        table[i][1]= '\0';
        ++i;
    }
#endif
}

static void printTable(TABLE_TYPE* table, int size)
{
    for (int i = 0 ; i < size ;)
    {
#ifdef NUMBER
        printf("%7.2lf", *(table + i));
#else
        printf("%3s", *(table + i));
#endif
        ++i;
    }
    printf("\n");
}

static double* getTable()
{
    lua_getglobal(L, "tbl");   
    GetTableSize(L, -1);
    return GetNumberTableFromStack(L);
}

int main(void)
{
    L = luaL_newstate();
    luaL_openlibs(L);

    if (luaL_loadfile(L, "tableApi.lua") != 0)
    {
        printf("Error loading .lua\n");
        return 1;
    }

    if (lua_pcall(L, 0, 0, 0))
    {
        printf("pcall failed");
        return 1;
    }

    TABLE_TYPE* table;
    Init(table);

    printTable(table, TABLE_SIZE);

    lua_getglobal(L, "transformTable");
#ifdef NUMBER
    PushNumberTableOnStack(L, table, TABLE_SIZE);
#else
    PushStringTableOnStack(L, table, TABLE_SIZE);
#endif

    if (lua_pcall(L, 1, 1, 0) != 0)
    {

        fprintf(stderr, "%s\n", lua_tostring(L, -1));
        return 1;
    }

#ifndef NUMBER
    for (int i = 0 ; i < TABLE_SIZE; i++)
    {
        free(table[i]);
    }
    table = GetStringTableFromStack(L);
#else
    free(table);
    table = GetNumberTableFromStack(L);
#endif

    printTable(table, TABLE_SIZE);
    free(table);
    lua_close(L);
    return 0;
}
