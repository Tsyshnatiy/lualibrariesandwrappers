#include "luatableapi.h"
#include <stdlib.h>

size_t GetTableSize(lua_State* L, int stackIdx)
{
    lua_len(L, stackIdx);
    size_t size = lua_tonumber(L, -1);
    lua_pop(L, 1);

    return size;
}

void PushNumberTableOnStack(lua_State* L,
                            const double* values,
                            const int valuesSize)
{
    lua_createtable(L, valuesSize, 0);

    for (int i = 0; i < valuesSize; )
    {
        lua_pushnumber(L, *(values + i));
        lua_rawseti(L, -2, i + 1);
        ++i;
    }
}

void PushStringTableOnStack(lua_State* L,
                            char** values,
                            const int valuesSize)
{
    lua_createtable(L, valuesSize, 0);

    for (int i = 0; i < valuesSize; )
    {
        lua_pushstring(L, *(values + i));
        lua_rawseti(L, -2, i + 1);
        ++i;
    }
}

double* GetNumberTableFromStack(lua_State* L)
{
    size_t sz = GetTableSize(L, -1);
    double* result = (double*) malloc(sz * sizeof(double));

    int i = 0;
    lua_pushnil(L);
    while(lua_next(L, -2))
    {
        result[i++] = lua_tonumber(L, -1);
        lua_pop(L, 1);
    }

    return result;
}

char** GetStringTableFromStack(lua_State* L)
{
    size_t sz = GetTableSize(L, -1);
    char** result = (char**) malloc(sz * sizeof(const char*));

    int i = 0;
    lua_pushnil(L);
    while(lua_next(L, -2))
    {
        result[i++] = const_cast<char*>(lua_tostring(L, -1));
        lua_pop(L, 1);
    }

    return result;
}
